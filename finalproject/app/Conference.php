<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
    protected $fillable = [
        'session',
		'email',
		'presentopt',
		'fees',
		'paymentopt',
		'paymentstatus',
		'attendence'
    ];

}
