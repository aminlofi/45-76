<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conference;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $conferences = Conference::all();
	   return view('conference.index', compact('conferences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('conference.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'session'=>'required',
            'email'=>'required',
			'presentopt'=>'required',
			'fees'=>'required',
			'paymentopt'=>'required',
			'paymentstatus'=>'required'
        ]);
		
		$conference = new Conference([
            'session' => $request->get('session'),
            'email' => $request->get('email'),
            'presentopt' => $request->get('presentopt'),
            'fees' => $request->get('fees'),
            'paymentopt' => $request->get('paymentopt'),
			'paymentstatus'=>$request->get('paymentstatus')
        ]);

		$conference->save();
        return redirect('/home')->with('success', 'Details saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conference = Conference::find($id);
        return view('conference.edit', compact('conference'));        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'paymentstatus'=>'required'
        ]);

        $conference = Conference::find($id);
        $conference->session =  $request->get('session');
        $conference->email = $request->get('email');
        $conference->presentopt = $request->get('presentopt');
        $conference->fees = $request->get('fees');
        $conference->paymentopt = $request->get('paymentopt');
        $conference->paymentstatus = $request->get('paymentstatus');
		$conference->attendence = $request->get('attendece');
        $conference->save();

        return redirect('/conference')->with('success', 'Details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$conference = Conference::find($id);
        $conference->delete();
				
        return redirect('/conference')->with('success', 'Details deleted!');
    }
}
