<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paper;

class PaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $papers = Paper::all();
		return view('paper.index', compact('papers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paper.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'paperbground'=>'required',
            'papertype'=>'required',
			'papername'=>'required',
            'email'=>'required'
        ]);

        $paper = new Paper([
            'paperbground' => $request->get('paperbground'),
            'papertype' => $request->get('papertype'),
            'papername' => $request->get('papername'),
            'email' => $request->get('email'),
			'status' => $request->get('status')
        ]);
		
		$paper->save();
        return redirect('/home')->with('success', 'Details saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paper = Paper::find($id);
        return view('paper.edit', compact('paper'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status'=>'required'
        ]);

        $paper = Paper::find($id);
        $paper->paperbground =  $request->get('paperbground');
		$paper->papertype =  $request->get('papertype');
		$paper->papername =  $request->get('papername');
		$paper->email =  $request->get('email');
		$paper->status =  $request->get('status');
        $paper->save();

        return redirect('/paper')->with('success', 'Details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paper = Paper::find($id);
        $paper->delete();
				
        return redirect('/paper')->with('success', 'Details deleted!');
    }
}
