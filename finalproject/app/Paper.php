<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    protected $fillable = [
        'paperbground',
		'papertype',
		'papername',
		'email',
		'status'
    ];

}
