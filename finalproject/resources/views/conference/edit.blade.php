@extends('base') 
@section('main')
<br>
<br>
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 style="text-align:center;"> Update Participant Status </h1>
		<hr>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('conference.update', $conference->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="session"> Session: </label>
                <input type="text" class="form-control" name="session" value={{ $conference->session }} readonly />
            </div>

            <div class="form-group">
                <label for="email"> Email: </label>
                <input type="text" class="form-control" name="email" value={{ $conference->email }} readonly />
            </div>

            <div class="form-group">
                <label for="presentopt"> Present Option: </label>
                <input type="text" class="form-control" name="presentopt" value={{ $conference->presentopt }} readonly />
            </div>
			
            <div class="form-group">
                <label for="city"> Fees: </label>
                <input type="text" class="form-control" name="fees" value={{ $conference->fees }} readonly />
            </div>
			
            <div class="form-group">
                <label for="paymentopt"> Payment Option: </label>
                <input type="text" class="form-control" name="paymentopt" value={{ $conference->paymentopt }} readonly />
            </div>
			
            <div class="form-group">
                <label for="paymentstatus"> Payment Status: </label>
                <br>
				<select name="paymentstatus">
					<option value="pending"> Pending </option>
					<option value="accept"> Accept </option>
				</select>
            </div>
			
			<div class="form-group">
				<label for="attendence"> Attendence: </label>
				<br>
				<select name="attendence">
					<option value=""> </option>
					<option value="attend"> Attend </option>
					<option value="not attend"> Not Attend </option>
				</select>
			</div>
			
            <button type="submit" class="btn btn-primary" style="margin:auto; display:block;">Update</button>
        
		</form>
    </div>
</div>
@endsection
