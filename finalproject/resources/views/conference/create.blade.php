@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
	<br>
	<br>
    <h1 style="text-align:center;">Join a conference</h1>
	<hr>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('conference.store') }}">
          @csrf
          
		  <div class="form-group">
              <label for="email"> Email: </label>
              <input type="text" class="form-control" name="email"/>
          </div>
		  
		  <div class="form-group">
              <label for="session"> Session: </label>
			  <br>
              <select name="session">
				<option value="indust4"> Indusry 4.0 and Beyond </option>
				<option value="businfo"> Business Informatics and Industry 4.0 </option>
				<option value="softemerge"> Software Systems and Emerging Technologies Communications Network and Industry 4.0 </option>
			  </select>
          </div>

          <div class="form-group">
				<label for="presentopt"> Present Option: </label>
				<br>
				<select name="presentopt">
					<option value="physical"> Physical Presentation </option>
					<option value="online"> Online Presentation </option>
				</select>
          </div>
		  
          <div class="form-group">
              <label for="fees"> Fees: </label>
			  <br>
			  <select name="fees">
					<option value="nonmember"> Normal (Non-Member IEEE)- RM2200 or USD530 </option>
					<option value="member"> IEEE Member - RM2000 or USD480 </option>
					<option value="student"> Student - RM1600 or USD385 </option>
			  </select>
          </div>
		
          <div class="form-group">
              <label for="paymentopt"> Payment Option: </label>
			  <br>
			  <select name="paymentopt">
					<option value="maybank"> Maybank2U </option>
					<option value="cimb"> CIMBClicks </option>
					<option value="tng"> TNG E Wallet </option>
			  </select>
          </div>

		  <input type="hidden" name="paymentstatus" value="pending"></input>
		  
		  <br>
          <button type="submit" class="btn btn-primary" style="margin:auto; display:block;"> Submit </button>
      
	  </form>
  </div>
</div>
</div>
@endsection
