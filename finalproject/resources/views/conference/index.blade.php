@extends('base')

@section('main')
<br>
<br>
<div class="row">
<div class="col-sm-12">
  <h1 style="text-align:center;">Participant Details</h1>
  <div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Session</td>
          <td>Email</td>
          <td>Present Option</td>
          <td>Fees</td>
          <td>Payment Option</td>
		  <td>Payment Status</td>
		  <td>Attendence</td>
		  <td> Update Status </td> 
		  <td> Reject Request </td> 
        </tr>
    </thead>
    <tbody>
        @foreach($conferences as $conference)
        <tr>
            <td> {{$conference->id}} </td>
            <td> {{$conference->session}} </td>
            <td> {{$conference->email}} </td>
            <td> {{$conference->presentopt}} </td>
            <td> {{$conference->fees}} </td>
            <td> {{$conference->paymentopt}} </td>
			<td><b style="color:gray;"> {{$conference->paymentstatus}} </b></td>
			<td> {{$conference->attendence}} </td>
			<td>
                <a href="{{ route('conference.edit',$conference->id)}}" class="btn btn-primary"> Edit </a>
            </td>

			
			<td>
                <form action="{{ route('conference.destroy', $conference->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit"> Remove </button>
                </form>
            </td>

			
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection
