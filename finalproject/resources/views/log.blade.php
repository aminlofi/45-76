@extends('base')
<br>
<br>
<h3 class="page-header" style="text-align:center;">CRS Log Files</h3>
<div class="row">
    <div class="col-sm-12">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>File Name</th>
                    <th>Size</th>
                    <th>Time</th>
                    <th>{{ trans('app.action') }}</th>
                </thead>
                <tbody>
                    @forelse($log as $key => $logs)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $logs->getFilename() }}</td>
                        <td>{{ $logs->getSize() }}</td>
                        <td>{{ date('Y-m-d H:i:s', $logs->getMTime()) }}</td>
                        <td>
                            <a href="{{ route('log.show', $logs->getFilename()) }}" title="Show file {{ $logs->getFilename() }}" style="color:pink;">View</a> 
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">No Log File Exists</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
    </div>
</div>
