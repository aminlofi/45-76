@extends('base')

@section('main')
<br>
<br>
<div class="row">
<div class="col-sm-12">
  <h1 style="text-align:center;"> Research Paper Details </h1>
  <div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td> ID </td>
          <td> Research Paper Background </td>
          <td> Research Paper Type </td>
          <td> Research Paper Topic </td>
		  <td> Email </td>
		  <td> Research Paper Status </td>
		  <td> Update Status </td>
		  <td> Reject Paper </td>	
        </tr>
    </thead>
    <tbody>
        @foreach($papers as $paper)
        <tr>
            <td> {{$paper->id}} </td>
            <td> {{$paper->paperbground}} </td>
            <td> {{$paper->papertype}} </td>
            <td> {{$paper->papername}} </td>
            <td> {{$paper->email}} </td>
            <td><b style="color:gray;"> {{$paper->status}} </b></td>
			<td>
                <a href="{{ route('paper.edit',$paper->id)}}" class="btn btn-primary"> Edit </a>
            </td>

			
			<td>
                <form action="{{ route('paper.destroy', $paper->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit"> Remove </button>
                </form>
            </td>
			
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection
