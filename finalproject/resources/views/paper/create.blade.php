@extends('papbase')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
	<br>
	<br>
    <h1 style="text-align:center;"> Upload Research Paper </h1>
	<hr>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('paper.store') }}">
          @csrf
		  <div class="form-group">
              <label for="papertype"> Research Publication: </label>
			  <br>
              <select name="papertype">
				<option value="journal"> IEE Journal </option>
				<option value="magazine"> IEEE Magazine </option>
				<option value="proceeding"> IEEE Conference Proceedings </option>
				<option value="book"> IEE Books </option>
			  </select>
          </div>
		  
          <div class="form-group">    
              <label for="paperbground"> Research Background: </label>
              <input type="text" class="form-control" name="paperbground"/>
          </div>

          <div class="form-group">
              <label for="papername"> Research Paper Topic: </label>
              <input type="text" class="form-control" name="papername"/>
          </div>
		  
		  <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email"/>
          </div>
		  
		  <input type="hidden" name="status" value="pending"></input>
		  
		  <br>
          <button type="submit" class="btn btn-primary" style="margin:auto; display:block;"> Submit </button>
      
	  </form>
  </div>
</div>
</div>
@endsection
