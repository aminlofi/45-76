@extends('base') 
@section('main')
<br>
<br>
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 style="text-align:center;"> Update Research Paper Status </h1>
		<hr>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('paper.update', $paper->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="paperbground"> Research Paper Background: </label>
                <input type="text" class="form-control" name="paperbground" value={{ $paper->paperbground }} readonly />
            </div>

            <div class="form-group">
                <label for="papertype"> Research Paper Type: </label>
                <input type="text" class="form-control" name="papertype" value={{ $paper->papertype }} readonly />
            </div>

            <div class="form-group">
                <label for="papername"> Research Paper Topic: </label>
                <input type="text" class="form-control" name="papername" value={{ $paper->papername }} readonly />
            </div>
			
            <div class="form-group">
                <label for="email"> Email: </label>
                <input type="text" class="form-control" name="email" value={{ $paper->email }} readonly />
            </div>
			
			<div class="form-group">
				<label for="status"> Research Paper Status: </label>
				<br>
				<select name="status">
					<option value="pending"> Pending </option>
					<option value="verified"> Verified </option>
				</select>
			</div>
		   
            <button type="submit" class="btn btn-primary" style="margin:auto; display:block;">Update</button>
        
		</form>
    </div>
</div>
@endsection
